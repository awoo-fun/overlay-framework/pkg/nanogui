package materialicons

import (
	"go.awoo.fun/of/pkg/nanovg"
)

func LoadFont(ctx *nanovg.Context) {
	ctx.CreateFontFromMemory("materialicons", MustAsset("font/MaterialIcons-Regular.ttf"), 0)
}

func LoadFontAs(ctx *nanovg.Context, name string) {
	ctx.CreateFontFromMemory(name, MustAsset("font/MaterialIcons-Regular.ttf"), 0)
}
