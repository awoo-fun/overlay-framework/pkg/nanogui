package nanogui

import (
	"go.awoo.fun/of/pkg/nanovg"
)

type Theme struct {
	StandardFontSize           int
	ButtonFontSize             int
	TextBoxFontSize            int
	WindowCornerRadius         int
	WindowHeaderHeight         int
	WindowDropShadowSize       int
	ButtonCornerRadius         int
	TabBorderWidth             float32
	TabInnerMargin             int
	TabMinButtonWidth          int
	TabMaxButtonWidth          int
	TabControlWidth            int
	TabButtonHorizontalPadding int
	TabButtonVerticalPadding   int

	DropShadow        nanovg.Color
	Transparent       nanovg.Color
	BorderDark        nanovg.Color
	BorderLight       nanovg.Color
	BorderMedium      nanovg.Color
	TextColor         nanovg.Color
	DisabledTextColor nanovg.Color
	TextColorShadow   nanovg.Color
	IconColor         nanovg.Color

	ButtonGradientTopFocused   nanovg.Color
	ButtonGradientBotFocused   nanovg.Color
	ButtonGradientTopUnfocused nanovg.Color
	ButtonGradientBotUnfocused nanovg.Color
	ButtonGradientTopPushed    nanovg.Color
	ButtonGradientBotPushed    nanovg.Color

	/* Window-related */
	WindowFillUnfocused  nanovg.Color
	WindowFillFocused    nanovg.Color
	WindowTitleUnfocused nanovg.Color
	WindowTitleFocused   nanovg.Color

	WindowHeaderGradientTop nanovg.Color
	WindowHeaderGradientBot nanovg.Color
	WindowHeaderSepTop      nanovg.Color
	WindowHeaderSepBot      nanovg.Color

	WindowPopup            nanovg.Color
	WindowPopupTransparent nanovg.Color

	FontNormal string
	FontBold   string
	FontIcons  string
}

func NewStandardTheme(ctx *nanovg.Context) *Theme {
	ctx.CreateFontFromMemory("sans", MustAsset("fonts/Roboto-Regular.ttf"), 0)
	ctx.CreateFontFromMemory("sans-bold", MustAsset("fonts/Roboto-Bold.ttf"), 0)
	ctx.CreateFontFromMemory("icons", MustAsset("fonts/entypo.ttf"), 0)
	return &Theme{
		StandardFontSize:           16,
		ButtonFontSize:             20,
		TextBoxFontSize:            20,
		WindowCornerRadius:         2,
		WindowHeaderHeight:         30,
		WindowDropShadowSize:       10,
		ButtonCornerRadius:         2,
		TabBorderWidth:             .75,
		TabInnerMargin:             5,
		TabMinButtonWidth:          20,
		TabMaxButtonWidth:          160,
		TabControlWidth:            20,
		TabButtonHorizontalPadding: 10,
		TabButtonVerticalPadding:   2,

		DropShadow:        nanovg.MONO(0, 128),
		Transparent:       nanovg.MONO(0, 0),
		BorderDark:        nanovg.MONO(29, 255),
		BorderLight:       nanovg.MONO(92, 255),
		BorderMedium:      nanovg.MONO(35, 255),
		TextColor:         nanovg.MONO(255, 160),
		DisabledTextColor: nanovg.MONO(255, 80),
		TextColorShadow:   nanovg.MONO(0, 160),
		IconColor:         nanovg.MONO(255, 160),

		ButtonGradientTopFocused:   nanovg.MONO(64, 255),
		ButtonGradientBotFocused:   nanovg.MONO(48, 255),
		ButtonGradientTopUnfocused: nanovg.MONO(74, 255),
		ButtonGradientBotUnfocused: nanovg.MONO(58, 255),
		ButtonGradientTopPushed:    nanovg.MONO(41, 255),
		ButtonGradientBotPushed:    nanovg.MONO(29, 255),

		WindowFillUnfocused:  nanovg.MONO(43, 230),
		WindowFillFocused:    nanovg.MONO(45, 230),
		WindowTitleUnfocused: nanovg.MONO(220, 160),
		WindowTitleFocused:   nanovg.MONO(255, 190),

		WindowHeaderGradientTop: nanovg.MONO(74, 255),
		WindowHeaderGradientBot: nanovg.MONO(58, 255),
		WindowHeaderSepTop:      nanovg.MONO(92, 255),
		WindowHeaderSepBot:      nanovg.MONO(29, 255),

		WindowPopup:            nanovg.MONO(50, 255),
		WindowPopupTransparent: nanovg.MONO(50, 0),

		FontNormal: "sans",
		FontBold:   "sans-bold",
		FontIcons:  "icons",
	}
}
